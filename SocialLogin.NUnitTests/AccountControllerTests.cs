﻿using NUnit.Framework;
using SocialLogin.API.Models;
using System;
using System.Net;
using System.Threading.Tasks;

namespace SocialLogin.NUnitTests
{
    [TestFixture]
    public class AccountControllerTests : BaseServerTest
    {
        [Test]
        public async Task CanRegisterUser()
        {
            var model = new RegisterExternalBindingModel
            {
                UserName = "faceBookExternalTest",
                Provider = "Facebook",
                Email = "test@test.test",
                ExternalAccessToken = "xxxxx"
            };

            uri = uriBase + "/registerexternal";

            var response = await PostAsync(model);

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        private string uriBase = "/api/account";
        private string uri = string.Empty;

        protected override string Uri
        {
            get { return uri; }
        }
    }
}