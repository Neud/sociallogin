﻿using NUnit.Framework;
using SocialLogin.API.Models;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace SocialLogin.NUnitTests
{
    [TestFixture]
    public class UsersControllerTests : BaseServerTest
    {
        [Test]
        public async Task ShouldGetUnauthorizedWithoutLogin()
        {
            var response = await GetAsync();

            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }

        protected override string Uri
        {
            get { return "/api/users"; }
        }
    }

    [TestFixture]
    public class UsersAuthenticatedInFacebookControllerTests : BaseAuthenticatedTests
    {
        public UsersAuthenticatedInFacebookControllerTests() 
            : base("Facebook", "xxxxx") { }

        [Test]
        public async Task ShouldGetValuesWhenAuthenticatedInFacebook()
        {
            var response = await GetAsync<IEnumerable<ExternalLoginViewModel>>();

            Assert.AreEqual(2, response.Count());
        }

        protected override string Uri
        {
            get { return "/api/users"; }
        }
    }

    [TestFixture]
    public class UsersAuthenticatedInLinkedInControllerTests : BaseAuthenticatedTests
    {
        public UsersAuthenticatedInLinkedInControllerTests()
            : base("LinkedIn", "xxxxx") { }

        [Test]
        public async Task ShouldGetValuesWhenAuthenticatedInLinkedIn()
        {
            var response = await GetAsync<IEnumerable<ExternalLoginViewModel>>();

            Assert.AreEqual(2, response.Count());
        }

        protected override string Uri
        {
            get { return "/api/users"; }
        }
    }
}