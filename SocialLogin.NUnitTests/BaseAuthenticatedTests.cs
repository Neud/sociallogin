﻿using Microsoft.Owin.Testing;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace SocialLogin.NUnitTests
{
    public abstract class BaseAuthenticatedTests : BaseServerTest
    {
        private string _provider;
        private string _externalAccessToken;
        private string _localToken;

        public BaseAuthenticatedTests(string provider, string externalAccessToken)
        {
            _provider = provider;
            _externalAccessToken = externalAccessToken;
        }

        protected override void PostSetup(TestServer server)
        {
            var tokenRequestUri = string.Format("/api/account/ObtainLocalAccessToken?provider={0}&externalaccesstoken={1}", _provider, _externalAccessToken);
            var tokenResult = server.HttpClient.GetAsync(tokenRequestUri).Result;
            Assert.AreEqual(HttpStatusCode.OK, tokenResult.StatusCode);

            var body = JObject.Parse(tokenResult.Content.ReadAsStringAsync().Result);

            _localToken = (string)body["access_token"];
        }

        protected async Task<TResult> GetAsync<TResult>()
        {
            var response = await GetAsync();
            return await response.Content.ReadAsAsync<TResult>();
        }

        protected override async Task<HttpResponseMessage> GetAsync()
        {
            return await server.CreateRequest(Uri)
                .AddHeader("Authorization", "Bearer " + _localToken)
                .GetAsync();
        }
    }
}