﻿using SocialLogin.API.Models;
using SocialLogin.API.Providers;
using AspNet.Identity.MongoDB;
using Autofac;
using Autofac.Builder;
using Autofac.Integration.WebApi;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Facebook;
using Microsoft.Owin.Security.OAuth;
using Owin;
using Owin.Security.Providers.LinkedIn;
using Swashbuckle.Application;
using System.IO;
using System.Web.Http;
using SocialLogin.API.MongoDB;

[assembly: OwinStartup(typeof(SocialLogin.API.Startup))]
namespace SocialLogin.API
{
    public class Startup
    {
        public static OAuthBearerAuthenticationOptions OAuthBearerOptions { get; private set; }
        public static FacebookAuthenticationOptions facebookAuthOptions { get; private set; }
        public static LinkedInAuthenticationOptions linkedinAuthOptions { get; private set; }

        public void Configuration(IAppBuilder app)
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<MongoContext>().AsImplementedInterfaces().SingleInstance();

            builder.RegisterType<AuthRepository>().SingleInstance();

            builder.RegisterType<ApplicationIdentityContext>()
                .SingleInstance();

            builder.RegisterType<UserStore<User>>()
                .AsImplementedInterfaces<IUserStore<User>, ConcreteReflectionActivatorData>()
                .SingleInstance();

            builder.RegisterType<RoleStore<Role>>()
                .AsImplementedInterfaces<IRoleStore<Role>, ConcreteReflectionActivatorData>()
                .SingleInstance();

            builder.RegisterType<ApplicationUserManager>()
                .SingleInstance();

            builder.RegisterType<ApplicationRoleManager>()
                .SingleInstance();

            builder.RegisterApiControllers(typeof(Startup).Assembly);

            var container = builder.Build();

            app.UseAutofacMiddleware(container);

            var webApiDependencyResolver = new AutofacWebApiDependencyResolver(container);

            var configuration = new HttpConfiguration
            {
                DependencyResolver = webApiDependencyResolver
            };

            ConfigureOAuth(app, container);

            WebApiConfig.Register(configuration);

            configuration
                .EnableSwagger(c =>
                {
                    c.SingleApiVersion("v1", "SocialLogin.API");
                    c.OperationFilter(() => new AddAuthorizationHeaderParameterOperationFilter());
                    c.IncludeXmlComments(Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, @"bin\SocialLogin.API.XML"));
                })
                .EnableSwaggerUi(c =>
                {
                    //c.CustomAsset()
                }); 

            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);

            app.UseWebApi(configuration);

            app.UseAutofacWebApi(configuration);
        }

        public void ConfigureOAuth(IAppBuilder app, IContainer container)
        {
            // Using a cookie to temporarily store information about a user logging in with a third party login provider
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            OAuthBearerOptions = new OAuthBearerAuthenticationOptions();

            // Token Generation
            app.UseOAuthBearerAuthentication(OAuthBearerOptions);

            //Configure Facebook External Login
            facebookAuthOptions = new FacebookAuthenticationOptions()
            {
                AppId = "xxxxx",
                AppSecret = "xxxxx",
                Provider = new FacebookAuthProvider()
            };
            app.UseFacebookAuthentication(facebookAuthOptions);

            //Configure LinkedIn External Login
            linkedinAuthOptions = new LinkedInAuthenticationOptions()
            {
                ClientId = "xxxxx",
                ClientSecret = "xxxxx",
                Provider = new LinkedInAuthProvider()
            };
            app.UseLinkedInAuthentication(linkedinAuthOptions);
        }
    }
}