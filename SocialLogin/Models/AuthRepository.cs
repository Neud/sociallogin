﻿using Microsoft.AspNet.Identity;
using SocialLogin.API.MongoDB;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialLogin.API.Models
{
    public class AuthRepository
    {
        private readonly IMongoContext mongoContext;
        private readonly ApplicationUserManager userManager;

        public AuthRepository(IMongoContext mongoContext, ApplicationUserManager userManager)
        {
            this.mongoContext = mongoContext;
            this.userManager = userManager;
        }

        public List<ExternalLoginViewModel> GetUsers()
        {
            var userData = mongoContext.Users.FindAllAs<User>().Select(x => new ExternalLoginViewModel
            {
                Name = x.UserName,
                Email = x.Email,
                Provider = (x.Logins != null && x.Logins.Count > 0) ? x.Logins.First().LoginProvider : null
            });

            return userData.ToList();
        }

        #region newToExternalServices

        public async Task<User> FindAsync(UserLoginInfo loginInfo)
        {
            return await userManager.FindAsync(loginInfo);
        }

        public async Task<IdentityResult> CreateAsync(User user)
        {
            return await userManager.CreateAsync(user);
        }

        public async Task<IdentityResult> AddLoginAsync(string userId, UserLoginInfo login)
        {
            return await userManager.AddLoginAsync(userId, login);
        }

        #endregion
    }
}