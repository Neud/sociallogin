﻿using System.ComponentModel.DataAnnotations;

namespace SocialLogin.API.Models
{
    /// <summary>
    /// API user
    /// </summary>
    public class ExternalLoginViewModel
    {
        /// <summary>
        /// Full name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Social network name
        /// </summary>
        public string Provider { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        public string Email { get; set; }
    }

    public class RegisterExternalBindingModel
    {
        /// <summary>
        /// Full name
        /// </summary>
        [Required]
        public string UserName { get; set; }

        /// <summary>
        /// Social network name
        /// </summary>
        [Required]
        public string Provider { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        [Required]
        public string Email { get; set; }

        /// <summary>
        /// Access token from social network
        /// </summary>
        [Required]
        public string ExternalAccessToken { get; set; }
    }

    public class ParsedExternalAccessToken
    {
        public string user_id { get; set; }
        public string app_id { get; set; }
    }
}