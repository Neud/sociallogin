﻿using SocialLogin.API.Models;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;

namespace SocialLogin.API.Controllers
{
    [RoutePrefix("api/Users")]
    public class UsersController : ApiController
    {
        private readonly AuthRepository authRepository = null;

        public UsersController(AuthRepository authRepository)
        {
            this.authRepository = authRepository;
        }

        /// <summary>
        /// Get all users
        /// </summary>
        /// <remarks>Get an array of all users</remarks>
        /// <response code="500">Internal Server Error</response>
        /// <returns>List of registered users</returns>
        [Authorize]
        [Route("")]
        [ResponseType(typeof(List<ExternalLoginViewModel>))]
        public IHttpActionResult Get()
        {
            return Ok(authRepository.GetUsers());
        }
    }
}