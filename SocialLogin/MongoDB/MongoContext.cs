using MongoDB.Bson;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Driver;
using SocialLogin.API.Models;
using System.Configuration;

namespace SocialLogin.API.MongoDB
{

    public class MongoContext : IMongoContext
    {
        private readonly MongoCollection<User> userCollection;
        private readonly MongoCollection<Role> roleCollection;

        public MongoContext()
        {
            var pack = new ConventionPack()
            {
                new CamelCaseElementNameConvention(),
                new EnumRepresentationConvention(BsonType.String)
            };

            ConventionRegistry.Register("CamelCaseConvensions", pack, t => true);

            var mongoUrlBuilder = new MongoUrlBuilder(ConfigurationManager.ConnectionStrings["AuthContext"].ConnectionString);

            var mongoClient = new MongoClient(mongoUrlBuilder.ToMongoUrl());
            var server = mongoClient.GetServer();

            Database = server.GetDatabase(mongoUrlBuilder.DatabaseName);

            userCollection = Database.GetCollection<User>("users");
            roleCollection = Database.GetCollection<Role>("roles");
        }

        public MongoDatabase Database { get; private set; }

        public MongoCollection<User> Users
        {
            get { return userCollection; }
        }

        public MongoCollection<Role> Roles
        {
            get { return roleCollection; }
        }
    }
}