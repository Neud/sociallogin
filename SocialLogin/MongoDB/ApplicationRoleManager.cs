using AspNet.Identity.MongoDB;
using Microsoft.AspNet.Identity;
using SocialLogin.API.Models;

namespace SocialLogin.API.MongoDB
{
    public class ApplicationRoleManager : RoleManager<Role>
    {
        public ApplicationRoleManager(ApplicationIdentityContext identityContext)
            : base(new RoleStore<Role>(identityContext))
        {
        }
    }
}