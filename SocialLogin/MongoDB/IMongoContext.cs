using MongoDB.Driver;
using SocialLogin.API.Models;

namespace SocialLogin.API.MongoDB
{
    public interface IMongoContext
    {
        MongoDatabase Database { get; }

        MongoCollection<User> Users { get; }
        MongoCollection<Role> Roles { get; }
    }
}