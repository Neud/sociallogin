using AspNet.Identity.MongoDB;
using Microsoft.AspNet.Identity;
using SocialLogin.API.Models;

namespace SocialLogin.API.MongoDB
{
    public class ApplicationUserManager : UserManager<User>
    {
        public ApplicationUserManager(ApplicationIdentityContext identityContext)
            : base(new UserStore<User>(identityContext))
        {
            UserValidator = new UserValidator<User>(this)
            {
                AllowOnlyAlphanumericUserNames = false
            };
        }
    }
}